package co.gov.desarrolloeconomico.modelo.vo;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase Que modela la tabla EMPRENDIMIENTO
 * @author Lmp
 *
 */
@Entity
@Table(name = "EMPRENDIMIENTO")
public class EmprendimientoVO {

	/*se generan los atributos que representan los campos de la tabla
	 * @id indica cual es el campo primario
	 * @GeneratedValue y @SequenceGenerator se empelan para indicar cual es la secuancia que 
	 *  llevara el contador en los campos primarios
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPR_ID_G")
    @SequenceGenerator(sequenceName = "S_EMPR_ID", allocationSize = 1, name = "EMPR_ID_G")
	@Column(name = "EMPR_ID")
	private int emprId;
	@Column(name = "EMPR_NOMBRES")
	private String emprNombres;
	@Column(name = "EMPR_NOMBRE_EMPRENDIMIENTO")
	private String emprNombreEmprendimiento;
	@Column(name = "EMPR_APELLIDOS")
	private String emprApellidos;
	@Column(name = "EMPR_DESCRIPCION")
	private String emprDescripcion;
	@Column(name = "EMPR_EMAIL")
	private String emprEmail;
	@Column(name = "EMPR_FECHA_NACIMIENTO" )
	private Date emprFechaNacimiento;
	@Column(name = "EMPR_FECHA_CREACION",insertable=false, updatable=false)
	private Date emprFechaCreacion;
	/**
	 * @JoinColumn y @ManyToOne modelan la relacion de uno a muchos entre las dos tablas
	 */
	@JoinColumn(name = "TEMP_ID", referencedColumnName = "TEMP_ID")
	
	@ManyToOne(optional = false)
	private TipoEmprendimientoVO tempId;
		
	public int getEmprId() {
		return emprId;
	}
	public void setEmprId(int emprId) {
		this.emprId = emprId;
	}
	public String getEmprNombres() {
		return emprNombres;
	}
	public void setEmprNombres(String emprNombres) {
		this.emprNombres = emprNombres;
	}
	public String getEmprApellidos() {
		return emprApellidos;
	}
	public void setEmprApellidos(String emprApellidos) {
		this.emprApellidos = emprApellidos;
	}
	public String getEmprDescripcion() {
		return emprDescripcion;
	}
	public void setEmprDescripcion(String emprDescripcion) {
		this.emprDescripcion = emprDescripcion;
	}
	public String getEmprEmail() {
		return emprEmail;
	}
	public void setEmprEmail(String emprEmail) {
		this.emprEmail = emprEmail;
	}

	public TipoEmprendimientoVO getTempId() {
		return tempId;
	}
	public void setTempId(TipoEmprendimientoVO tempId) {
		this.tempId = tempId;
	}
	public Date getEmprFechaNacimiento() {
		return emprFechaNacimiento;
	}
	public void setEmprFechaNacimiento(Date emprFechaNacimiento) {
		this.emprFechaNacimiento = emprFechaNacimiento;
	}
	public Date getEmprFechaCreacion() {
		return emprFechaCreacion;
	}
	public void setEmprFechaCreacion(Date emprFechaCreacion) {
		this.emprFechaCreacion = emprFechaCreacion;
	}
	public String getEmprNombreEmprendimiento() {
		return emprNombreEmprendimiento;
	}
	public void setEmprNombreEmprendimiento(String emprNombreEmprendimiento) {
		this.emprNombreEmprendimiento = emprNombreEmprendimiento;
	}
	
}
