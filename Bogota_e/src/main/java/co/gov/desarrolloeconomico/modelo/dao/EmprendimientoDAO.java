package co.gov.desarrolloeconomico.modelo.dao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import co.gov.desarrolloeconomico.modelo.vo.EmprendimientoVO;

/**
 * Secrean la interfaz para acceso a los datos, esta hereda de 
 * JpaRepository, la cual contiene los metodos basicos para realizar el CRUD
 * @author Lmp
 *
 */
public interface EmprendimientoDAO extends JpaRepository<EmprendimientoVO,Integer> {
	/**
	 * Se crea la funcion para consultar por el campo que almacena el nombre del emprendimiento
	 * para ello el nombre de la funcion secrea con el estandar Spring Data que es:
	 *  lapalabra findBy seguido del nombre del campo por el cual se buscara, y se pasa el parametro
	 *  de busqueda
	 * @param emprNombreEmprendimiento
	 * @return
	 */
	public List<EmprendimientoVO> findByEmprNombreEmprendimiento(String emprNombreEmprendimiento);  
}
