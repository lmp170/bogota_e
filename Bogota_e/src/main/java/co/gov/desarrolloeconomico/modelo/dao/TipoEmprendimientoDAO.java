package co.gov.desarrolloeconomico.modelo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import co.gov.desarrolloeconomico.modelo.vo.TipoEmprendimientoVO;

/**
 * Secrean la interfaz para acceso a los datos, esta hereda de 
 * JpaRepository, la cual contiene los metodos basicos para realizar el CRUD
 * @author Lmp
 *
 */
public interface TipoEmprendimientoDAO extends JpaRepository<TipoEmprendimientoVO, Integer> {

}
