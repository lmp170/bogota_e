package co.gov.desarrolloeconomico.modelo.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * Clase Que modela la tabla TIPO_EMPRENDIMIENTO
 * @author Lmp
 *
 */
@Entity
@Table(name = "TIPO_EMPRENDIMIENTO")
public class TipoEmprendimientoVO  {

		
	/*se generan los atributos que representan los campos de la tabla
	 * @id indica cual es el campo primario
	 * @GeneratedValue y @SequenceGenerator se empelan para indicar cual es la secuancia que 
	 *  llevara el contador en los campos primarios
	 */
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TMEP_ID_G")
	    @SequenceGenerator(sequenceName = "S_TEMP_ID", allocationSize = 1, name = "TMEP_ID_G")
	    @Column(name = "TEMP_ID")		
		private int tempId;
		@Column(name = "TEMP_DESCRIPCION")
		private String tempDescripcion;
		
		
		public int getTempId() {
			return tempId;
		}
		public void setTempId(int tempId) {
			this.tempId = tempId;
		}
		public String getTempDescripcion() {
			return tempDescripcion;
		}
		public void setTempDescripcion(String tempDescripcion) {
			this.tempDescripcion = tempDescripcion;
		}
				
		
}
