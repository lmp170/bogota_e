package co.gov.desarrolloeconomico.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.gov.desarrolloeconomico.modelo.dao.EmprendimientoDAO;
import co.gov.desarrolloeconomico.modelo.vo.EmprendimientoVO;

/**
 * Se crea el controlador el cual mapeara las rutas de los servicios REST
 * @author Lmp
 *
 */


 
@RestController
@RequestMapping("/emprendimiento")
public class EmprendimientoControlador {
	@Autowired

        
        private EmprendimientoDAO dao;
	/**
	 * se mapea el metodo get para el servicio, este retornara los elementos de la tabla 
	 * Emprendimiento,en formato JSON, pero en este caso van ordenados por el campo que 
	 * almacena la fecha de creacion
	 * @return
	 */
	@GetMapping
	public List<EmprendimientoVO> listar(){
		return dao.findAll(Sort.by(Sort.Direction.ASC, "emprFechaCreacion"));
	}
	/**
	 *  * Se mapea el metodo post el cual realizara la insercion,
	 * Recibe como parametro un objeto EmprendimientoVO, mediante una peticion JSON
	 * la cual es procesada  gracias al tag @RequestBody, el JSON debe traer los atributos
	 * igual como se declararon en la clase EmprendimientoVO
	 * @param vo
	 */
	@PostMapping
	public void insertar(@RequestBody EmprendimientoVO vo) {
		dao.save(vo);
	}
	/**
	 * Se mapea el metodo put el cual perite la edicion de los registros
	 * funciona igual que el metodo POST pero se activa cuando la clave primaria trae contenido
	 * con un dato que existe
	 * @param vo
	 */
	@PutMapping
	public void modificar(@RequestBody EmprendimientoVO vo) {
		dao.save(vo);
	}
	/**
	 * Mapea el metodo get pero recibiendo un parametro, en este caso es el 
	 * nombre del emprendimiento
	 * el tag @PathVariable indica a la funcion que el parametro recivido sera recibido en el String
	 * @param nombre nombre del emprendimiento que se desea buscar
	 * @return
	 */
	@GetMapping(value="/{nombre}")
	public List<EmprendimientoVO> buscarXNombreEmprendimiento(@PathVariable("nombre") String nombre){
		
		return dao.findByEmprNombreEmprendimiento(nombre);
	}
	

}
