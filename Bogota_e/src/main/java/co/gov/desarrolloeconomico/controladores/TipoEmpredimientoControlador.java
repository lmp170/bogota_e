package co.gov.desarrolloeconomico.controladores;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.gov.desarrolloeconomico.modelo.dao.TipoEmprendimientoDAO;
import co.gov.desarrolloeconomico.modelo.vo.TipoEmprendimientoVO;


/**
 * Se crea el controlador el cual mapeara las rutas de los servicios REST
 * @author Lmp
 *
 */
@RestController
@RequestMapping("/tipoEmprendimiento")
public class TipoEmpredimientoControlador {
	/**
	 * declaramos un objeto de la interfaz TipoEmprendimientoDAO el cual se instancia medeiante
	 * @Autowired, este tag asigna una unica instancia al objeto, basado en el patron sigleton
	 * a o que tbn se le conoce como inyecion de dependencias
	 */
	@Autowired
	private TipoEmprendimientoDAO dao;
	
	/**
	 * se mapea el metodo get para el servicio, este retornara los elementos de la tabla 
	 * Tipo_emprendimiento,en formato JSON
	 * @return
	 */
	@GetMapping
	public List<TipoEmprendimientoVO> listar() {
		return dao.findAll();
	}
	/**
	 * Se mapea el metodo post el cual realizara la insercion,
	 * Recibe como parametro un objeto TipoEmprendimientoVO, mediante una peticion JSON
	 * la cual es procesada  gracias al tag @RequestBody, el JSON debe traer los atributos
	 * igual como se declararon en la clase TipoEmprendimientoVO
	 * 
	 * @param vo
	 */
	@PostMapping
	public void insertar(@RequestBody TipoEmprendimientoVO vo) {
		//el metodo save ejecuta el insert con la informacion que llega en la peticion 
		dao.save(vo);
	}
	
	/**
	 * Se mapea el metodo put el cual perite la edicion de los registros
	 * funciona igual que el metodo POST pero se activa cuando la clave primaria trae contenido
	 * con un dato que existe
	 * @param vo
	 */
	@PutMapping
	public void modificar(@RequestBody TipoEmprendimientoVO vo) {
		dao.save(vo);
	}
}
