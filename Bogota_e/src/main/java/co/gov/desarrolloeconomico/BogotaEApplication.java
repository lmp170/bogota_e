package co.gov.desarrolloeconomico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BogotaEApplication {

	public static void main(String[] args) {
		SpringApplication.run(BogotaEApplication.class, args);
	}

}
